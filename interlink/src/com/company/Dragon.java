package com.company;

/**
 * Created by Mykhailo on 12/13/2016.
 */
public class Dragon extends Animal implements Birds,Squamata {

    private double dragonSpeed;
    private double eagleWingspan;
    private double eagleAltitude;
    private  boolean layEggs;


    public Dragon(){
        super("None","None");
    }

    public Dragon(String aName, String aKind) {
        super(aName, aKind);
    }

    @Override
    public void Fly(double eSpeed) { // принцим поліформізму
    System.out.println("The dragon flys");
    }

    @Override
    public void Hunt() {
        System.out.println("The dragon hunts");
    }

    @Override
    public void Runs(double sSpeed) {
        System.out.println("The dragon runs");
        dragonSpeed = sSpeed;
    }

    @Override
    public void LayEggs() {
        System.out.println("The dragon lays eggs");
        layEggs = true;
    }

    @Override
    public void Eat() {
        System.out.println("The dragon eats");
    }

    @Override
     public void Sleep() {
        System.out.println("The dragon sleeps");
    }

    public void BreathingFire(){
        System.out.println("The dragon breathings fire");
    }

    public void LikesGold(){
        System.out.println("The dragon likes gold");
    }
}
