package com.company;

/**
 * Created by Mykhailo on 12/13/2016.
 */
abstract public class Animal {
        private String animalName; // ілюструється принцип інкапсуляції
        private String animalKind;

        protected Animal(String aName, String aKind){
            animalName = aName;
            animalKind = aKind;
        }

        public void setName(String aName){
            animalName = aName;
        }

        public String getName(){
            return animalName;
        }

        public  void setKind(String aKind){
            animalKind  = aKind;
        }

        public String getKind(){
            return animalKind;
        }

        abstract void Eat();
        abstract void Sleep();
    }
