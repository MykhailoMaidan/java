

package com.company;



public class Main {

    public static void main(String[] args) {

	Animal eagleTom = new Eagle(); // приклад поліморфізму

	eagleTom.setName("Tom");
	eagleTom.setKind("Birds");
	eagleTom.Sleep();

	System.out.println("Later.");

	eagleTom.Eat();

	System.out.println("Name  eagl:" + eagleTom.getName());


	Birds eagleJack = new Eagle("Jack","Birds","Lviv",78.5); // приклад поліморфізму

	eagleJack.Fly(25.1);

	System.out.println("Later");

	eagleJack.Hunt();

	Eagle eagleMiki = new Eagle();
	eagleMiki.setName("Miki");
	eagleMiki.setKind("Birds");

	eagleMiki.Eat();
	System.out.println("Later");
	eagleMiki.Fly(25.1);

	System.out.println("Kind eagle:" + eagleMiki.getKind());

	Dragon dragonSmaug = new Dragon("Smaug","mythical");

	dragonSmaug.BreathingFire();

	dragonSmaug.LikesGold();

	Squamata dragonKirk = new Dragon("Kirk", "mythical");
	dragonKirk.LayEggs();
	dragonKirk.Runs(254.5);





    }
}
