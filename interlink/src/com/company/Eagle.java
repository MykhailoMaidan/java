package com.company;

/**
 * Created by Mykhailo on 12/13/2016.
 */
public class Eagle extends Animal implements Birds { // принцим наслідування абстрактного класу і інтерфейсу


    private double speedEagle;
    private double eagleWingspan;
    private double eagleAltitude;
    private String eagleHabitat;


    public Eagle(){
        super("None","None");
    }

    public Eagle(String aName, String aKind,String aHabitat,
                 double aWingspan) {
        super(aName, aKind);
        this.eagleWingspan = aWingspan;
        this.eagleHabitat = aHabitat;
    }

    @Override
     public void Eat() {
        System.out.println("The eagle eat a meat.");
    }

    @Override
    public void Sleep() {
        System.out.print("The eagle sleep.");
    }


    @Override
    public void Fly(double eSpeed) {
        System.out.println("The eagle  fly.");
        if(eagleAltitude > 1000){
            speedEagle = 250;
        }else{
            speedEagle = 150;
        }
    }

    @Override
    public void Hunt() {
        System.out.println("The eagle  hunts.");
    }
}
