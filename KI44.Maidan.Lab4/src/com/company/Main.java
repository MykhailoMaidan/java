package com.company;

import TV.TVTuner.TVTuner;
import TV.TunerInterface.TunerInterface;
import TV.TV;
import TV.TvTunerT2;

/**
 * В даному проекті я реазілував принципи ООП
 * Створив абстрактний клас Телевізор від нього наслідував клас  Телевізор з тюнером який реалізував функціонал базового класу
 * Підклас також розширюється за допомогою інтерфейсу Тюнер
 *
 *
 */
public class Main {

    public static void main(String[] args) throws Throwable {
	// write your code here
        TVTuner obj = new TVTuner();
        obj.TurnOff(true); // приклад поліморфізму
        obj.SoundLevelHigh();
        obj.SoundLevelLow();
        obj.ChannelTvHigh();
        obj.ChannelTvLow();
        obj.ChoiseRegimTv();
        obj.RecordTV();
        obj.PlayTV();
        obj.SetCabel(TV.cabelTv.Analog);

        TvTunerT2 tuner = new TvTunerT2(TvTunerT2.regimTuner.FMRadio);
        tuner.ForwardTV();


        TunerInterface testTuner = new TVTuner(); // поліморфний обєкт

        testTuner.RecordTV(); // приклад поліморфізму
        testTuner.ForwardTV();


        TV smartTv = new TVTuner();
        smartTv.TurnOff(true);
        smartTv.SoundLevelLow();
        smartTv.ChannelTvLow();


        obj.finalize();


    }
}
