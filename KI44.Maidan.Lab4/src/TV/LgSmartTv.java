package TV;


import TV.TV.cabelTv;

import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Mykhailo on 12/26/2016.
 */
public class LgSmartTv extends TV
{

    public enum tvGames{Mario, Poisen};
    public enum tvWeather {Sunny,Cloudy};
    protected tvGames mGame;
    protected tvWeather mWeather;
    protected boolean stepAhead;
    protected boolean stepBack;
    protected boolean upHero;
    protected boolean downHero;


    public LgSmartTv(tvWeather sWether) throws FileNotFoundException {
        super();
        mWeather = sWether;
    }

    @Override
    public int SoundLevelHigh() {
        if(regimTV != regimTv.SmartTv){
        if (soundLevel <= maxSoundLevel) {
            soundLevel++;
            return soundLevel;
        } else {
            return 100;
        }
    }else{
            upHero = true;
            return 1;
        }
    }

    @Override
    public int SoundLevelLow() {
        if (regimTV != regimTv.SmartTv) {
            if (soundLevel <= 0) {
                return 0;
            } else {
                soundLevel--;
                return soundLevel;
            }
        }else{
            downHero = true;
            return 0;
        }
    }

    @Override
    public int ChannelTvHigh() {
        if(regimTV != regimTv.SmartTv){
        if (channelTv == maxChannelTv) {
            channelTv = 1;
            return channelTv;
        } else {
            channelTv++;
            return channelTv;
            }
        }else{
            stepAhead = true;
            return 1;
        }
    }

    @Override
    public int ChannelTvLow() {
        if (regimTV != regimTv.SmartTv) {
            if (channelTv == 1) {
                channelTv = maxChannelTv;
                return channelTv;
            } else {
                channelTv--;
                return channelTv;
            }
        } else {
            stepBack = true;
            return 0;
        }
    }

    @Override
    public void ChoiseRegimTv() {

        System.out.println("Choise regim TV: 1 - TV, 2 - USB" +
                "3 - Video,  4 - SmartTV");
        Scanner in = new Scanner(System.in);
        int choise_regim = in.nextInt();
        in.nextLine();

        switch (choise_regim) {
            case 1: {
                regimTV = regimTV.tv;
                break;
            }
            case 2: {
                regimTV = regimTv.usb;
                break;
            }
            case 3: {
                regimTV= regimTV.video;
                break;
            }
            case 4: {
                regimTV = regimTV.SmartTv;
                break;
            }
            default: {
                regimTV = regimTV.tv;
                break;
            }
        }
    }

    public void SetGame(tvGames sGame){
            this.mGame = sGame;
    }

    public tvWeather GetWeather(){
            return mWeather;
    }
}
