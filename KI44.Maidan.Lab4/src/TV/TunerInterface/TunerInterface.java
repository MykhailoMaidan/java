package TV.TunerInterface;

/**
 * Created by Mykhailo on 9/20/2016.
 * Interface
 * @author Mykhailo Maidan
 * @version 1.0
 */
 public  interface TunerInterface {
   /**
    * Method for starts record video
    */
    public void RecordTV();

   /**
    * Method stop the video
    */
    public void PausTV();

   /**
    * Method play the video
    */
    public void PlayTV();

   /**
    *Method rewinds the video forward
    */
    public void ForwardTV();

   /**
    * Method rewinds the video back
    */
    public void BackTV();

   /**
    * Method Switches forward
    */
    public void NextTV();

   /**
    * Method Switches previous
    */
    public void PreviousTV();

}
