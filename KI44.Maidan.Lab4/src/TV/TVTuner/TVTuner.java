package TV.TVTuner;

import TV.TunerInterface.TunerInterface;
import TV.TV;

import java.io.FileNotFoundException;
import java.util.Scanner;


public class TVTuner extends TV implements TunerInterface { // наслідування абстрактного класу і інтерфейсу

    private boolean recordTV;
    private boolean playTV;
    private boolean pausTV;
    private int nextTV;
    private int previousTV;
    private double forwardTV;
    private double backTV;


    public TVTuner() throws FileNotFoundException {
        super();
        nextTV = 0;
        previousTV = 0;
        forwardTV = 0;
        backTV = 0;
        playTV = false;
        pausTV = true;
        recordTV =  false;
    }

    @Override
    public int SoundLevelHigh() {

        if (soundLevel <= maxSoundLevel) {
            soundLevel++;
            fileWrite.println("Sound level:" + soundLevel);
            return soundLevel;
        } else {

            fileWrite.println("Sound level is max.");
            return 100;
        }

    }


    @Override
    public int SoundLevelLow() {
        if (soundLevel <= 0) {

            fileWrite.println("Sound level:" + soundLevel);
            return 0;
        } else {
            soundLevel--;
            fileWrite.println("Sound level is min.");
            return soundLevel;
        }
    }




    @Override
    public void TurnOffSound() {
        soundLevel = 0;
        fileWrite.println("Sound is turn off.");
    }


    @Override
    public int ChannelTvHigh() {
        if (channelTv == maxChannelTv) {
            channelTv = 1;
            fileWrite.println("Channel:" + channelTv);
            return channelTv;
        } else {
            channelTv++;
            fileWrite.println("Channel:" + channelTv);
            return channelTv;
        }
    }


    @Override
    public int ChannelTvLow() {
        if (channelTv == 1) {
            channelTv = maxChannelTv;
            fileWrite.println("Channel:" + channelTv);
            return channelTv;
        } else {
            channelTv--;
            fileWrite.println("Channel:" + channelTv);
            return channelTv;
        }
    }




    public void ChoiseRegimTv() {
        System.out.println("Choise regim TV: 1 - TV, 2 - USB" +
                "3 - Video, 4 - PC, 5 - SmartTV");
        Scanner in = new Scanner(System.in);
        int choise_regim = in.nextInt();
        in.nextLine();

        switch (choise_regim) {
            case 1: {
                fileWrite.println("Regim is change" + regimTv.tv);
                regimTV = regimTV.tv;
                break;
            }
            case 2: {
                fileWrite.println("Regim is change" + regimTV.usb);
                regimTV = regimTv.usb;
                break;
            }
            case 3: {
                fileWrite.println("Regim is change" + regimTV.video);
                regimTV= regimTV.video;
                break;
            }
            case 4: {
                fileWrite.println("Regim is change" + regimTV.pc);
                regimTV = regimTV.pc;
                break;
            }
            case 5: {
                fileWrite.println("Regim is change" + regimTV.SmartTv);
                regimTV = regimTV.SmartTv;
                break;
            }
            default: {
                regimTV = regimTV.tv;
                break;
            }
        }

    }


    @Override
    public void RecordTV() {
        recordTV = true;
        fileWrite.println("There:recordTVTV");
    }

    @Override
    public void PausTV() {
        pausTV = true;
        playTV = false;
        fileWrite.println("Tuner:pausTV");
    }

    @Override
    public void PlayTV() {
        pausTV = false;
        playTV = true;
        fileWrite.println("Tuner:playTV");
    }

    @Override
    public void ForwardTV() {
        forwardTV += 1.1;
        backTV = forwardTV;
        fileWrite.println("Tuner:forwardTV");
    }

    @Override
    public void BackTV() {
        backTV -= 1.1;
        forwardTV  = backTV;
        fileWrite.println("Tuner:backTV");
    }

    @Override
    public void NextTV() {
        nextTV++;
        previousTV = nextTV;
        fileWrite.println("Tuner:nextTV");
    }

    @Override
    public void PreviousTV() {
        previousTV--;
        nextTV = previousTV;
        fileWrite.println("Tuner:previousTV");
    }

        public void finalize() throws Throwable  {
       super.finalize();
            fileWrite.flush();
            fileWrite.close();

        }



}


