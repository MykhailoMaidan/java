package TV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;



abstract public class TV { // абстрактний клас
    public enum  regimTv{
        tv,
        usb,
        video,
        pc,
        SmartTv
    }
    public enum cabelTv{
        Analog,
        Digital
    }

    protected   boolean turnOffOn;

    protected regimTv regimTV;
    protected cabelTv cabelChoise;
    protected cabelTv mCabelTv;
    protected int soundLevel;
    protected int maxSoundLevel;
    protected int channelTv;
    protected int maxChannelTv;
    protected   File in;
    protected PrintWriter fileWrite;


    public TV() throws FileNotFoundException{
        turnOffOn = false;
        regimTV = regimTv.tv;
        cabelChoise = cabelTv.Analog;
        in = new File("TV.txt");
        fileWrite = new PrintWriter(in);
        fileWrite.println("TV is create");
    }
    
    public TV(boolean mTurnOffOn, cabelTv m_cabel) throws FileNotFoundException {
        turnOffOn = mTurnOffOn;
        soundLevel = 50;
        channelTv = 1;
        maxChannelTv = 100;
        maxSoundLevel  = 100;
        regimTV = regimTv.tv;
        in = new File("TV.txt");
        fileWrite = new PrintWriter(in);
        fileWrite.println("TV is create");
        cabelChoise = m_cabel;
    }

     public boolean TurnOff(boolean turn){
        if (turn == true) {
            System.out.println("Welcome to TV");
            fileWrite.println("TV has been turn on");
            turnOffOn = true;
            return true;
        } else {
            return false;
        }
    }

     public void SoundLevelLow(int sLevel){
         soundLevel = sLevel;
     }

     public void TurnOffSound(){
        soundLevel = 0;
        fileWrite.println("Sound is turn off.");
    }

    public  void SetChannelTv(int mСhannelTv){
        channelTv = mСhannelTv;
    }
     public void SetCabel(cabelTv mСabel){
        cabelChoise = mСabel;
        fileWrite.println("Regim is cabel" + cabelChoise);
    }

    abstract public int SoundLevelHigh();
    abstract public int SoundLevelLow();
    abstract public int ChannelTvHigh();
    abstract public int ChannelTvLow();
    abstract public void ChoiseRegimTv();
}
