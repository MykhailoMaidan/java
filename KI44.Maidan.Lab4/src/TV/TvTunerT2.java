package TV;

import TV.TunerInterface.TunerInterface;

/**
 * Created by Mykhailo on 12/26/2016.
 */
public class TvTunerT2 implements TunerInterface {


    public enum regimTuner {FMRadio,TV};
    protected regimTuner mRegimTunerm;
    protected boolean playTv;
    protected boolean pauseTv;
    protected boolean forwardTv;
    protected boolean backTv;
    protected boolean recordTv;
    protected boolean nextTv;
    protected boolean previousTv;
    protected boolean playFM;
    protected boolean pauseFM;
    protected boolean forwardFM;
    protected boolean backFM;
    protected boolean recordFM;
    protected boolean nextFM;
    protected boolean previousFM;

    public TvTunerT2(regimTuner sReginTuner){
        this.mRegimTunerm = sReginTuner;
    }
    @Override
    public void RecordTV() {
        if(mRegimTunerm == regimTuner.TV){
            recordTv = true;
            recordFM = false;
        }else{
            recordTv = false;
            recordFM = true;
        }
    }

    @Override
    public void PausTV() {
        if(mRegimTunerm == regimTuner.TV){
            pauseTv = true;
            pauseFM = false;
        }else{
            pauseTv = false;
            pauseFM = true;
        }
    }

    @Override
    public void PlayTV() {
        if(mRegimTunerm == regimTuner.TV){
            playTv = true;
            playFM = false;
        }else{
            playTv = false;
            playFM = true;
        }
    }

    @Override
    public void ForwardTV() {
        if(mRegimTunerm == regimTuner.TV){
            forwardTv = true;
            forwardFM = false;
        }else{
            forwardTv = false;
            forwardFM = true;
        }
    }

    @Override
    public void BackTV() {
        if(mRegimTunerm == regimTuner.TV){
            backTv = true;
            backFM = false;
        }else{
            backTv = false;
            backFM = true;
        }
    }

    @Override
    public void NextTV() {
        if(mRegimTunerm == regimTuner.TV){
            nextTv = true;
            nextFM = false;
        }else{
            nextTv = false;
            nextFM = true;
        }
    }

    @Override
    public void PreviousTV() {
        if(mRegimTunerm == regimTuner.TV){
            previousTv = true;
            previousFM = false;
        }else{
            previousTv = false;
            previousFM = true;
        }
    }
}
