package com.maidan.work;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mykhailo on 12/31/2016.
 */
public class Calendar {


    private final Integer NUMBER_OF_TODAY = 2; // кількість елементів у масиві де зберігається рядок та стовпець поточної дати
    private final Integer WEEKEND_DAY = 5; // вихідні дні починаються від 5 дня тиждня
    private final byte FIRST_DAY_OF_MONTH = 1; // перший день місяця
    private final byte DAYS_OF_WEEK = 7; // кількість днів у тиждні
    private String calendarColor = (char) 27 + "[39m"; // загальний коляр для календаря
    private String todayColor = (char) 27 + "[36m"; // коляр для поточного дня у місяці
    private String weekendColor = (char) 27 + "[33m"; // соляр для вихідних днів
    private byte[] nowaDay; // розмішення поточної дати, 0 елемент - рядокь, 1 елемент - стовпець у матриці
    private boolean isToday = false;    // прапорець для перевірки чи вказаний місяць є поточним
    private LocalDate dataYearMonth; // об'єкт для роботи за датами
    private List<List<Byte>> dataCalendar;// двовимірний масив для формування календаря

    /*
    * конструктор
    * по замовчуванняю працюємо з поточною датою
    */
    public Calendar() {
        dataYearMonth = LocalDate.now();
        dataCalendar = new ArrayList<>();
    }

    public void getCalendar(int iYear, int iMonth) {
        dataCalendar.clear();
        isToday = false;
        if (!isYear(iYear) && !isMonth(iMonth)){
            iMonth = LocalDate.now().getMonthValue();
            iYear = LocalDate.now().getYear();

        } else if (!isYear(iYear)) {
            iYear = LocalDate.now().getYear();

        } else if (!isMonth(iMonth)) {
            iMonth = LocalDate.now().getMonthValue();
        }

        CreateCalendar(iYear,iMonth);
    }

    private boolean isYear(int iYear){
        return iYear > 0 ? true :false;
    }

    private boolean isMonth( int iMonth){
        return iMonth <13 && iMonth >0 ? true : false;
    }

    private void CreateCalendar(int iYear, int iMonth) {

        dataYearMonth = LocalDate.of(iYear, iMonth, FIRST_DAY_OF_MONTH); // ініціалізація обєкта дати
        List<Byte> tempData; // масив для заповнення масиму календаря

        // різниця між початком тиждня і днем першого числа
        int counterDays = FIRST_DAY_OF_MONTH - dataYearMonth.getDayOfWeek().getValue();

        while (counterDays < dataYearMonth.getMonth().length(dataYearMonth.isLeapYear())) {

            tempData = new ArrayList<>();

            while (tempData.size() != DAYS_OF_WEEK) {
                ++counterDays;

                if (counterDays < 0 || counterDays > dataYearMonth.getMonth().length(dataYearMonth.isLeapYear())) {
                    tempData.add((byte) 0);

                } else if (dataYearMonth.getMonth() == LocalDate.now().getMonth() && counterDays == LocalDate.now().getDayOfMonth()
                        && dataYearMonth.getYear() == LocalDate.now().getYear()) {

                    nowaDay = new byte[NUMBER_OF_TODAY];
                    tempData.add((byte) counterDays);
                    nowaDay[0] = (byte) (dataCalendar.size());
                    nowaDay[1] = (byte) (tempData.size()-1);
                    isToday = true;

                }else{
                    tempData.add((byte) counterDays);
                }
            }
            dataCalendar.add(tempData);
        }
    }
    /*
    Метод для виводу календаря
     */
    public void PrintCalendar() {

        System.out.println("Year: " + dataYearMonth.getYear() + ", Month: " + dataYearMonth.getMonthValue());
        System.out.print("Mo\tTu\tWe\tTh\tFr\t"+weekendColor + "Sa\tSu\n");

        for (int i = 0; i < dataCalendar.size(); i++) {

            for (int j = 0; j < dataCalendar.get(i).size(); j++) {

                if(dataCalendar.get(i).get(j) == 0){
                    System.out.print("\t");
                } else if (isToday && nowaDay[0] == i && nowaDay[1] == j) {
                    System.out.print(todayColor + dataCalendar.get(i).get(j) + calendarColor + "\t");
                } else if (j>=WEEKEND_DAY) {
                    System.out.print(weekendColor + dataCalendar.get(i).get(j) + calendarColor + "\t");
                } else {
                    System.out.print(calendarColor + dataCalendar.get(i).get(j)  + calendarColor + "\t");
                }
            }
            System.out.println();
        }
    }
}
