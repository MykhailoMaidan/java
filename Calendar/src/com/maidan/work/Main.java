package com.maidan.work;

import java.time.LocalDate;
import java.util.Scanner;


public class Main {


    private static String enterYear;// зміння для вводу року
    private static String enterMonth; // змінна для вводу місяця
    private static Integer iYear; // введений рік
    private static Integer iMonth; // введений місяць
    private static String selectNumber; // змінна для отриманна цифри - вибору у меню
    private static  Scanner scannerData = new Scanner(System.in);
    private static String continueWork =("Y"); // відзначення чи працювати далі чи ні
    private static  Calendar calendar = new Calendar();

    public static void main(String[] args) {

        while (continueWork.equals("Y") || continueWork.equals("y")) {
            MenuSelect();
            ScannerData();
                calendar.getCalendar(iYear, iMonth);
                calendar.PrintCalendar();
            System.out.print("Continue work, enter Y/N:  ");
            continueWork = scannerData.nextLine();
        }

    }

    private static void MenuSelect() {

        System.out.print("Please make your selection:\n" +
                "1 - select the year and month,\n" +
                "2 - exit.\n"+"You choice:");
    }

    private static void ScannerData() {
        try {
            selectNumber = scannerData.nextLine();
            switch (Integer.valueOf(selectNumber)) {
                case 1: {
                    try {
                        System.out.print("Enter year:");
                        enterYear = scannerData.nextLine();
                        System.out.print("Enter month:");
                        enterMonth = scannerData.nextLine();
                        if (enterYear.length() == 0 || enterMonth.length() == 0) {
                            iYear = LocalDate.now().getYear();
                            iMonth = LocalDate.now().getMonthValue();
                        } else {
                            iYear = Integer.valueOf(enterYear.toString());
                            iMonth = Integer.valueOf(enterMonth.toString());
                        }
                    } catch (NumberFormatException e) {
                        iYear = LocalDate.now().getYear();
                        iMonth = LocalDate.now().getMonthValue();
                    }
                    break;
                }
                case 2: {
                    System.exit(1);
                    break;
                }
            }
        } catch (NumberFormatException e) {
            System.exit(1);
        }
    }
}





