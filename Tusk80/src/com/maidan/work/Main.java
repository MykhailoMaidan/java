package com.maidan.work;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static String[] treeList;
    static ArrayList<String> treeDirectory = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        RecursDir("D:\\");
        for(String x:treeDirectory){
            System.out.println(x.toString());
        }

    }

    public static void RecursDir(String path) {
        File dir = new File(path);
        File[] temp = dir.listFiles();
        if (temp != null) {
            for (File x : temp) {
                if (x.isFile()) {
                    treeDirectory.add("L:" + x);
                } else {
                    treeDirectory.add("D------------:" + x);
                    RecursDir(x.getAbsolutePath()   );
                }
            }
        }
    }
}
