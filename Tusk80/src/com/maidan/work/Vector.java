package com.maidan.work;


import sun.text.normalizer.UCharacter;

import java.util.Objects;

/**
 * Created by Mykhailo on 12/15/2016.
 */
public class Vector<T extends Number> {
   private T []massElement;
   private int position;

    public Vector  (int number){
        massElement = (T[])new Object[number];
        position = 0;
    }

    public void Add(T element){
        massElement[position++] = element;

    }
}
